# -*- coding: utf-8 -*-
import scrapy
from douban.items import DoubanItem


class DoubanSpider(scrapy.Spider):
    name = 'Douban'
    allowed_domains = ['book.douban.com']
    start_urls = ['http://book.douban.com/']

    def parse(self, response):
        for s in response.xpath('//div[@class="carousel"]/div[@class="slide-list"]/ul/li'):
            item = DoubanItem()
            item['title'] = s.xpath(
                'div[@class="info"]/div[@class="title"]/a/text()').extract_first().strip()
            item['link'] = s.xpath(
                'div[@class="info"]/div[@class="title"]/a/@href').extract_first().strip()
            item['author'] = s.xpath(
                'div[@class="info"]/div[@class="author"]/text()')[0].extract().strip()
            item['abstract'] = s.xpath(
                'div[@class="info"]/div[@class="more-meta"]/p[@class="abstract"]/text()')[0].extract().strip()
            item['time'] = s.xpath(
                'div[@class="info"]/div[@class="more-meta"]/p/span[@class="year"]/text()')[0].extract().strip()
            yield item
